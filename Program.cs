﻿using System;

namespace dotnetcore
{
    class Program
    {
        static void Main(string[] args)
        {
            int length = 100000;
            SummatorBench sb = new SummatorBench(length);
            TimeSpan plainBenchFor = sb.CalcSumPlainFor();
            TimeSpan plainBenchForEach = sb.CalcSumPlainForEach();
            TimeSpan threadBench = sb.CalcSumTread();
            TimeSpan plinqAsLongBench = sb.CalcSumPLinqAsLong();
            TimeSpan plinqBench = sb.CalcSumPLinq();
            TimeSpan linqBench = sb.CalcSumLinq();
            Console.WriteLine($"Plain (for) sum time: {FormatDate(plainBenchFor)}");
            Console.WriteLine($"Plain (foreach)sum time: {FormatDate(plainBenchForEach)}");
            Console.WriteLine($"Thread sum time: {FormatDate(threadBench)}");
            Console.WriteLine($"PLINQ sum time: {FormatDate(plinqBench)}");
            Console.WriteLine($"LINQ sum time: {FormatDate(linqBench)}");
            Console.WriteLine($"PLINQ as long sum time: {FormatDate(plinqAsLongBench)}");
        }
        private static string FormatDate(TimeSpan ts) => string.Format("{0:00}:{1:00}:{2:00}.{3:00}  Ticks {4}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10, ts.Ticks);



    }
}
