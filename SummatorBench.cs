﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace dotnetcore
{
    public class SummatorBench
    {
        private readonly int[] _arr;
        private int _arrayLength;
        private object _lock = new object();
        private long _sum;
        private int _threadsCount;

        public SummatorBench(int arrayLength)
        {
            _arrayLength = arrayLength;
            _arr = new int[_arrayLength];
            FillArray();
            _threadsCount = 2;
        }

        private void FillArray()
        {
            for (int i = 0; i < _arrayLength; i++)
            {
                _arr[i] = 1;
            }
        }

        public TimeSpan CalcSumPlainFor()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            long sum = 0;
            for (int i = 0; i < _arrayLength; i++)
            {
                sum += _arr[i];
            }
            sw.Stop();
            Console.WriteLine($"Calculated sum: {sum}");
            return sw.Elapsed;
        }

        public TimeSpan CalcSumPlainForEach()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            long sum = 0;
            foreach (var item in _arr)
            {
                sum += item;
            }
            sw.Stop();
            Console.WriteLine($"Calculated sum: {sum}");
            return sw.Elapsed;
        }

        public TimeSpan CalcSumTread()
        {
            Stopwatch sw = new Stopwatch();
            _sum = 0;
            sw.Start();
            Thread[] threads = new Thread[_threadsCount];
            for (int i = 0; i < _threadsCount; i++)
            {
                threads[i] = new Thread(CalcThread);
                threads[i].Start(i);
            }
            for (int i = 0; i < _threadsCount; i++)
            {
                threads[i].Join();
            }
            sw.Stop();
            Console.WriteLine($"Calculated sum: {_sum}");
            return sw.Elapsed;
        }

        private void CalcThread(object startIndex)
        {
            long sum = 0;
            int itemsByThread = _arrayLength / _threadsCount;
            for (int i = (int)startIndex * itemsByThread; i < ((int)startIndex + 1) * itemsByThread; i++)
            {
                sum += _arr[i];
            }
            lock (_lock)
            {
                _sum += sum;
            }
        }

        public TimeSpan CalcSumPLinqAsLong()
        {
            Stopwatch sw = new Stopwatch();
            _sum = 0;
            sw.Start();
            _sum = _arr.AsParallel().Sum(x => (long)x);
            sw.Stop();
            Console.WriteLine($"Calculated sum: {_sum}");
            return sw.Elapsed;
        }

        public TimeSpan CalcSumPLinq()
        {
            Stopwatch sw = new Stopwatch();
            _sum = 0;
            sw.Start();
            _sum = _arr.AsParallel().Sum();
            sw.Stop();
            Console.WriteLine($"Calculated sum: {_sum}");
            return sw.Elapsed;
        }

        public TimeSpan CalcSumLinq()
        {
            Stopwatch sw = new Stopwatch();
            _sum = 0;
            sw.Start();
            _sum = _arr.Sum();
            sw.Stop();
            Console.WriteLine($"Calculated sum: {_sum}");
            return sw.Elapsed;
        }
    }
}
